package com.clientui.beans;

import java.util.Date;

public class CreditcartPaymentBean extends PaymentBean {
    private int cardNumber;
    private String dateExpiration;

    public CreditcartPaymentBean() {
        super();
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getDateExpiration() {
        return dateExpiration;
    }

    public void setDateExpiration(String dateExpiration) {
        this.dateExpiration = dateExpiration;
    }
}
